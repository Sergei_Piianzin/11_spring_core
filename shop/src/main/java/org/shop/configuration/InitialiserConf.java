package org.shop.configuration;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value="org.shop")
public class InitialiserConf {

  @Bean
  public Map<Long, String> sellerNames() {
    Map<Long, String> sellerNames = new HashMap<>();
    sellerNames.put(1L, "Google");
    sellerNames.put(2L, "Yandex");
    sellerNames.put(3L, "SpaseX");
    sellerNames.put(4L, "AMD");
    sellerNames.put(5L, "Intel");
    return sellerNames;
  }
}
