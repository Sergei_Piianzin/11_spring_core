package org.shop.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value="org.shop.api.impl")
public class ServiceConf {
  
}
