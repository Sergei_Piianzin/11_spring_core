package org.shop.configuration;

import org.shop.repository.UserRepository;
import org.shop.repository.factory.UserRepositoryFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.shop.repository.factory")
public class FactoryConf {

  @Bean
  public UserRepository userRepository() {
    return new UserRepositoryFactory().createUserRepository();
  }
}
