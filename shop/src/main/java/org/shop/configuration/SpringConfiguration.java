package org.shop.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({FactoryConf.class, RepositoryConf.class, ServiceConf.class, InitialiserConf.class})
public class SpringConfiguration {

}
