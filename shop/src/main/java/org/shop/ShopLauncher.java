package org.shop;

import java.util.List;
import java.util.logging.Logger;

import org.shop.api.OrderService;
import org.shop.api.ProposalService;
import org.shop.api.SellerService;
import org.shop.api.UserService;
import org.shop.api.impl.OrderServiceImpl;
import org.shop.api.impl.ProductServiceImpl;
import org.shop.api.impl.ProposalServiceImpl;
import org.shop.api.impl.SellerServiceImpl;
import org.shop.data.Proposal;
import org.shop.data.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The ShopLauncher class.
 */
public class ShopLauncher {
  private static Logger logger = Logger.getLogger(ShopLauncher.class.toString());

  /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
      ApplicationContext context = new ClassPathXmlApplicationContext("java_conf.xml");
      SellerService sellerService = context.getBean(SellerServiceImpl.class);
      ProposalService proposalService = context.getBean(ProposalServiceImpl.class);
      OrderService orderService = context.getBean(OrderServiceImpl.class);
      UserService userService = context.getBean(UserService.class);
      User user = userService.getUsers().get(0);
      List<Proposal> proposals = proposalService.getProposalsBySeller(sellerService.getSellerById(((long)1)));
      long orderId = orderService.createOrder(user, proposals.get(0));
      logger.info(sellerService.getSellers().toString());
      logger.info(orderService.getOrderById(orderId).toString());
    }
}
